"""
streamlit的插件，可以实现登陆功能。https://github.com/mkhorasani/Streamlit-Authenticator
"""
import streamlit as st
import streamlit_authenticator as stauth

import yaml
from yaml.loader import SafeLoader
# hashed_passwords = stauth.Hasher(['abc']).generate()  用来生成密码的哈希，然后讲hash填到yml里面就可以
# print(hashed_passwords)
# exit()
with open('./config.yaml') as file:
    config = yaml.load(file, Loader=SafeLoader)

authenticator = stauth.Authenticate(
    config['credentials'],
    config['cookie']['name'],
    config['cookie']['key'],
    config['cookie']['expiry_days'],
    config['preauthorized']
)
authenticator.login('请登陆', 'sidebar')
# hashed_passwords = stauth.Hasher(['abc', 'def']).generate()
# print(hashed_passwords)
if st.session_state["authentication_status"]:
    authenticator.logout('Logout', 'main', key='unique_key')
    st.write(f'Welcome *{st.session_state["name"]}*')
    st.title('Some content')
elif st.session_state["authentication_status"] is False:
    st.error('Username/password is incorrect')
elif st.session_state["authentication_status"] is None:
    st.warning('Please enter your username and password')