"""
工具文件
"""
from datetime import datetime
import pandas as pd
from sqlalchemy import create_engine, text


def get_rb_trade_data(conn, table_trade, table_signal, schema_name, start_time=0.0) -> (pd.DataFrame, pd.DataFrame):
    """
    从mysql数据库获取rb策略的交易与信号数据

    :return: pd.DataFrame
    """

    sql_signal = f"SELECT * FROM {schema_name}.{table_signal} WHERE time > {start_time}"
    sql_trade = f"SELECT * FROM {schema_name}.{table_trade} WHERE trade_date_time > {start_time * 1e9}"
    signal = pd.read_sql(text(sql_signal), conn)
    trade = pd.read_sql(text(sql_trade), conn)
    return signal, trade


if __name__ == '__main__':
    engine = create_engine("mysql+pymysql://root:TODO@111.230.63.142/signal_order_trade_fake")
    # conn = engine.connect()
    get_rb_trade_data(engine, 'trades', 'strategy_signal')