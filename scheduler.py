import os
from apscheduler.schedulers.background import BackgroundScheduler
import time


def job():
    print('do')
    os.popen('docker start thirsty_wing')


scheduler = BackgroundScheduler()
scheduler.add_job(job, 'cron', day_of_week='mon-fri', hour=18, minute=7, timezone='Asia/Shanghai')
scheduler.start()
if __name__ == '__main__':
    while True:
        time.sleep(1)